#!/bin/ash

# Based on: http://www.richud.com/wiki/Ubuntu_Fluxbox_GUI_with_x11vnc_and_Xvfb

readonly G_LOG_I='[INFO]'
readonly G_LOG_W='[WARN]'
readonly G_LOG_E='[ERROR]'

main() {
    rm -f /tmp/.X1-lock
    rm -f /tmp/.X0-lock
    run_vnc_server
    dbus &
    run_pulseaudio &
    launch_window_manager
    wait_and_resize_chrome &
    [ -z "${DISABLE_VIDEO_STREAM}" ] && run_stream &
    [ -z "${DISABLE_AUDIO_STREAM}" ] && run_audio_stream &
    wait $!
}

run_pulseaudio() {
  while :
  do
    pulseaudio
  done
}

wait_and_resize_chrome() {
    local resolution=${XVFB_RESOLUTION:-1280x720}
    screen -d -m chromium-browser
    until xdotool search --name ".* - Chromium" > /dev/null 2>&1
    do
        sleep 1
    done
    xdotool search --name ".* - Chromium" windowsize ${resolution/x/ }
    xdotool search --name ".* - Chromium" windowmove 0 0
}

current_window_manager() {
   id=`xprop -root -notype | awk '$1=="_NET_SUPPORTING_WM_CHECK:"{print $5}'`
   WM=`xprop -id $id -notype -f _NET_WM_NAME 8t`
   echo $WM
}

launch_window_manager() {
    local timeout=${XVFB_TIMEOUT:-5}

    # Start and wait for either fluxbox to be fully up or we hit the timeout.
    fluxbox &
    local loopCount=0
    until echo `current_window_manager` | grep -o 'Fluxbox'
    do
        echo `current_window_manager`
        loopCount=$((loopCount+1))
        sleep 1
        if [ ${loopCount} -gt ${timeout} ]
        then
            echo "${G_LOG_E} fluxbox failed to start."
            exit 1
        fi
    done
}

run_stream() {
        local stream_url=""
        bitrate=${VIDEO_BITRATE:-3000K}
        local resolution=${XVFB_RESOLUTION:-1280x720}

        if [ -n "${STREAM_URL}" ]
        then
            echo "${G_LOG_I} Stream URL: ${STREAM_URL}"
        else
            echo "STREAM_URL must be provided as an environment variable."
            echo "'-e STREAM_URL=icecast://...'"
            exit 1
        fi
        while :
        do
            ffmpeg -f alsa -thread_queue_size 32 -i default \
            -f x11grab -s ${resolution} -framerate 60 \
            -draw_mouse 0 -i $DISPLAY -vsync 1 \
            -c:v libvpx-vp9 \
            -crf 0 -qmin 0 -qmax 56 \
            -b:v ${bitrate} -bufsize ${bitrate} -minrate:v ${bitrate} \
            -maxrate:v ${bitrate} \
            -r 60 -pix_fmt yuv420p -map_metadata -1 -quality realtime \
            -deadline realtime -g 120 -tile-columns 5 -row-mt 1 -frame-parallel 1 -threads 12 -cpu-used 8 \
            -c:a libvorbis \
            -f webm -content_type video/webm -cluster_size_limit 4M \
            -cluster_time_limit 5000 \
            $STREAM_URL
        done
}

run_audio_stream() {
        local stream_url=""
        bitrate=${AUDIO_BITRATE:-320K}
        if [ -n "${STREAM_URL}" ]
        then
            echo "${G_LOG_I} Stream URL: ${STREAM_URL}"
        else
            echo "STREAM_URL must be provided as an environment variable."
            echo "'-e STREAM_URL=icecast://...'"
            exit 1
        fi
        while :
        do
            ffmpeg -f alsa -thread_queue_size 32 -i default \
            -c:a libvorbis -content_type 'audio/ogg' \
            -vn -f ogg \
            ${STREAM_URL/.webm/.ogg}
        done
}

run_vnc_server() {
    export DISPLAY=${XVFB_DISPLAY:-:0}
    local screen=${XVFB_SCREEN:-0}
    local resolution=${XVFB_RESOLUTION:-1280x720}
    local timeout=${XVFB_TIMEOUT:-5}

    local passwordArgument='-SecurityTypes None'

    if [ -n "${VNC_SERVER_PASSWORD}" ]
    then
        local passwordFilePath="${HOME}/x11vnc.pass"
        if ! vncpasswd -storepasswd "${VNC_SERVER_PASSWORD}" "${passwordFilePath}"
        then
            echo "${G_LOG_E} Failed to store x11vnc password."
            exit 1
        fi
        passwordArgument=-"-rfbauth ${passwordFilePath}"
        echo "${G_LOG_I} The VNC server will ask for a password."
    else
        echo "${G_LOG_W} The VNC server will NOT ask for a password."
    fi
      Xvnc ${DISPLAY} -geometry ${resolution} -depth 24 ${passwordArgument} &
    local loopCount=0
    until xdpyinfo -display ${DISPLAY} > /dev/null 2>&1
    do
        loopCount=$((loopCount+1))
        sleep 1
        if [ ${loopCount} -gt ${timeout} ]
        then
            echo "${G_LOG_E} X failed to start."
            exit 1
        fi
    done

}

control_c() {
    echo ""
    exit
}

trap control_c SIGINT SIGTERM SIGHUP

main

exit
