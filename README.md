This is docker image built due to the untimely demise of `rabb.it`
It will stream a Chrome window to an icecast server for group viewing.

Based on the following article: https://medium.com/dot-debug/running-chrome-in-a-docker-container-a55e7f4da4a8

Requirements:

- You must have an icecast2 server running. 
- To cast to an icecast2 server running on port `8000` on `localhost` with a username of `source`, and a password of `hackme`
 - STREAM_URL=`icecast://source:hackme@localhost:8000/stream.webm`
 - Your stream would be visible on "http://localhost:8000/stream.webm"

Usage:

```docker run -p 5900:5900 -e STREAM_URL=icecast://... --shm-size=1g --user apps --privileged binaryoutlook/streamer```

Once the container is running, connect to VNC on port 5900 and stream.

Currently Icecast must be patched to allow larger than normal EBML Headers due to a change in modern FFMPEG.

```diff

diff --git a/src/format_ebml.c b/src/format_ebml.c
index 1e2353f..07def71 100644
--- a/src/format_ebml.c
+++ b/src/format_ebml.c
@@ -789,11 +789,12 @@ static ssize_t ebml_wrote(ebml_t *ebml, size_t len)
                 }

                 if (ebml->parse_state == EBML_STATE_COPYING_TO_HEADER) {
+/*
                     if ((ebml->header_position + to_copy) > EBML_HEADER_MAX_SIZE) {
                         ICECAST_LOG_ERROR("EBML Header too large, failing");
                         return -1;
                     }
-
+*/
                     memcpy(ebml->header + ebml->header_position, ebml->input_buffer + cursor, to_copy);
                     ebml->header_position += to_copy;


```
