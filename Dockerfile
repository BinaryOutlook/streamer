# Create fresh image from alpine
FROM alpine:edge

RUN chmod 1777 /tmp && chmod 1777 /var/tmp

RUN adduser --disabled-password --gecos "" --no-create-home apps

RUN mkdir -p /home/apps && chown apps:apps /home/apps

#RUN apt-get install -y tigervnc-standalone-server
RUN apk add mesa-dri-gallium dbus nettle dbus-x11 nettle xkbcomp gnutls pixman libxfont2 libxau libxdmcp mesa-gl libjpeg libstdc++ libgcc && cd / && wget -O- https://binaryoutlook.com/tigervnc-alpine-Linux-`arch`-1.10.80.tar.gz | tar -xzvf -
RUN apk add xdpyinfo

#RUN apt-get install -y xvfb 
RUN apk add xorg-server

#RUN apt-get install -y nano 
RUN apk add nano

#RUN apt-get install -y pulseaudio 
RUN apk add pulseaudio alsa-utils pulseaudio-alsa pulseaudio-utils alsa-plugins-pulse

#RUN apt-get install -y mpv 
RUN apk add chromium
#RUN apt-get install -y wget 

#RUN apt-get install -y python 

#RUN apt-get install -y screen 
RUN apk add screen
#RUN apt-get install -y fluxbox 
RUN apk add fluxbox

#RUN apt-get install -y wmctrl 
#RUN apk add wmctrl

#RUN apt-get install -y xdotool 
RUN apk add xdotool
RUN apk add xprop
#RUN apt-get install -y software-properties-common

# Set the Chrome repo.
#RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
#    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

# Install Chrome and ffmpeg.
#RUN apt-get update && apt-get -y install google-chrome-stable ffmpeg
RUN apk add ffmpeg

RUN dbus-uuidgen > /var/lib/dbus/machine-id

COPY bootstrap.sh /

ENTRYPOINT '/bootstrap.sh'
